package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SimpleSwags.SimpleSwagsServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SimpleSwagsService.Op1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SimpleSwagsService.Op1ReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class Op1Tests {

	@Test
	public void testOperationOp1BasicMapping()  {
		SimpleSwagsServiceDefaultImpl serviceDefaultImpl = new SimpleSwagsServiceDefaultImpl();
		Op1InputParametersDTO inputs = new Op1InputParametersDTO();
		inputs.setMyElement(new MyElement());
		Op1ReturnDTO returnValue = serviceDefaultImpl.op1(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}