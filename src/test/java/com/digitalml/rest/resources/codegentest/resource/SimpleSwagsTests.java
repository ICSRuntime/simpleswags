package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class SimpleSwagsTests {

	@Test
	public void testResourceInitialisation() {
		SimpleSwagsResource resource = new SimpleSwagsResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationOp2NoSecurity() {
		SimpleSwagsResource resource = new SimpleSwagsResource();
		resource.setSecurityContext(null);

		Response response = resource.op2(new MyElement());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationOp1NoSecurity() {
		SimpleSwagsResource resource = new SimpleSwagsResource();
		resource.setSecurityContext(null);

		Response response = resource.op1(new MyElement());
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}