package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for MyElement:
{
  "required": [
    "Element2",
    "Element1"
  ],
  "type": "object",
  "properties": {
    "Element1": {
      "type": "string"
    },
    "Element2": {
      "type": "string"
    }
  }
}
*/

public class MyElement {

	@Size(max=1)
	@NotNull
	private String element1;

	@Size(max=1)
	@NotNull
	private String element2;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    element1 = org.apache.commons.lang3.StringUtils.EMPTY;
	    element2 = org.apache.commons.lang3.StringUtils.EMPTY;
	}
	public String getElement1() {
		return element1;
	}
	
	public void setElement1(String element1) {
		this.element1 = element1;
	}
	public String getElement2() {
		return element2;
	}
	
	public void setElement2(String element2) {
		this.element2 = element2;
	}
}