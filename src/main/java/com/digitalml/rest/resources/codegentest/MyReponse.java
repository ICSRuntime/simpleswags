package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for MyReponse:
{
  "required": [
    "ResponseElement1",
    "ResponseElement2"
  ],
  "type": "object",
  "properties": {
    "ResponseElement1": {
      "type": "string"
    },
    "ResponseElement2": {
      "type": "string"
    }
  }
}
*/

public class MyReponse {

	@Size(max=1)
	@NotNull
	private String responseElement1;

	@Size(max=1)
	@NotNull
	private String responseElement2;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    responseElement1 = org.apache.commons.lang3.StringUtils.EMPTY;
	    responseElement2 = org.apache.commons.lang3.StringUtils.EMPTY;
	}
	public String getResponseElement1() {
		return responseElement1;
	}
	
	public void setResponseElement1(String responseElement1) {
		this.responseElement1 = responseElement1;
	}
	public String getResponseElement2() {
		return responseElement2;
	}
	
	public void setResponseElement2(String responseElement2) {
		this.responseElement2 = responseElement2;
	}
}